<style TYPE="text/css">
code.has-jax {font: inherit; font-size: 100%; background: inherit; border: inherit;}
</style>
<script type="text/x-mathjax-config">
MathJax.Hub.Config({
    tex2jax: {
        inlineMath: [['$','$'], ['\\(','\\)']],
        skipTags: ['script', 'noscript', 'style', 'textarea', 'pre'] // removed 'code' entry
    }
});
MathJax.Hub.Queue(function() {
    var all = MathJax.Hub.getAllJax(), i;
    for(i = 0; i < all.length; i += 1) {
        all[i].SourceElement().parentNode.className += ' has-jax';
    }
});
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS_HTML-full"></script>

# TODOs for stacy, the C^ compiler

## Preprocessor
- expression whitespace removal
- includes using substitution

## STDLIB
- Arithmetik
   * division
   * multiplikation
   * modulo

`$$\sum_{i = 1}^{n} i = \frac{n(n+1)}{2}$$`
$$\sum_{i = 1}^{n} i = \frac{n(n+1)}{2}$$