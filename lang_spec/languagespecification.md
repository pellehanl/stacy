# Language specification for the C^ (C-Hat, Chad)

## General rules

- no unicode, only unsigned 8bit ASCII
- no formatting specification, free formatting

## Specification

- strong, static type system
- two basic numeric types only `I8` and `I16`
- other datatypes: pointers, lists, structs, sprites
- control flow: if, while, for (preprocessor)
- caller/callee -> function argument limitation

## Standard library
- allocator
- multiplication division
- charset?
- graphics operation


### Details

#### Optimizations
- tail recursion optimization (avoid recursion)

#### loops
- iteration variables in registers?

#### Lists
- two kinds of lists?

#### Allocator
- use a very simple allocator: [2B: Block size n | n bytes of data | 2B prev. block | 2B: next block]



  S      Heap        Stack                 Misc.
[...|>..........|.............<|............................]


