module ChadParser where
import Text.ParserCombinators.Parsec
import Text.ParserCombinators.Parsec.Expr
import Control.Monad

data Expr = Add Expr Expr |
            Sub Expr Expr | 
            Mul Expr Expr | 
            Div Expr Expr | 
            Mod Expr Expr |
            Assign Expr Expr |
            Val Int |
            Neg Expr | 
            Var String| 
            Index Expr Expr | 
            PreDec Expr | 
            PostDec Expr | 
            PreInc Expr | 
            PostInc Expr |
            Eq Expr Expr |
            Greater Expr Expr |
            Smaller Expr Expr |
            Neq Expr Expr |
            Call String [Expr] | 
            StrLit String |
            --Ret Expr |
            Unspecified deriving Show

data CType = I8 | I8P | I16 | I16P | V0 deriving (Show, Eq, Read)
data TypedIdentifier = TI CType String deriving (Show, Eq, Read)
data FunctionSkeleton = FS TypedIdentifier [TypedIdentifier] deriving (Show, Eq)


data Instruction = VD CType [Expr] | INEX Expr deriving Show

data CFlow = WH Expr [CFlow] |
             FOR Instruction Instruction Instruction [CFlow] |
             FUN FunctionSkeleton [CFlow] |
             Line Instruction | 
             ST String [Instruction] | 
             RET Expr deriving Show

type Program = [CFlow]

str :: Parser String
str = many1 (digit <|> letter)

int :: Parser Int
int = read <$> many1 digit

white1 :: Parser String
white1 = many1 (oneOf " \t")
white  = many  (oneOf " \t")

ctype :: Parser CType
ctype = read <$> many1 (oneOf "I816V0*") -- dirty hack to test FIXME: only allow correct types

typedIdentifier :: Parser TypedIdentifier
typedIdentifier = TI <$> ctype <* white1 <*> many1 (letter <|> digit) <* white

functionArguments :: Parser [TypedIdentifier]
functionArguments = many ((optional (char ',') *> white *>) typedIdentifier)

functionSkeleton :: Parser FunctionSkeleton
functionSkeleton = FS <$> typedIdentifier <* char '(' <*> functionArguments <* char ')'

table :: [[Operator Char st Expr]]
table =
  [ [ binary "=" Assign AssocLeft], [binary "!=" Neq AssocLeft], [binary "<" Smaller AssocLeft], [binary ">" Greater AssocLeft], [binary "?=" Eq AssocLeft]
  , [ prefix "--" PreDec], [ postfix "--" PostDec], [postfix "++" PostInc], [prefix "++" PreInc]
  , [ prefix "-" Neg ]
  , [ binary "*" Mul AssocLeft, binary "/" Div AssocLeft, binary "%" Mod AssocLeft ]
  , [ binary "+" Add AssocLeft, binary "-" Sub AssocLeft ]
  ] where binary  s f = Infix   (try (string s) >> return f)
          prefix  s f = Prefix  (try (string s) >> return f)
          postfix s f = Postfix (try (string s) >> return f)

val :: Parser Expr
val = Val <$> int

bracket :: Parser a -> Parser a
bracket p = white *> char '{' *> white *> p <* white <* char '}'

var :: Parser Expr
var = Var <$> many1 (digit <|> letter)

index :: Parser Expr
index = Index . Var <$> str <* white <* char '[' <* white <*> expr <* white <* char ']'

factor :: Parser Expr
factor = try call <|> try index <|> (char '(' *> buildExpressionParser table factor <* char ')') <|> val <|> var <|> strLit

expr :: Parser Expr
expr = buildExpressionParser table factor

assign :: Parser Expr
assign = Assign . Var <$> (white *> str) <*> (white *> char '=' *> white *> expr <* optional (char ',') <* white)

noassign :: Parser Expr
noassign = Var <$> (white *> str <* white <* optional ( char ',') <* white)

variableDefs :: Parser Instruction
variableDefs = VD <$> ctype <*> many1 (try assign <|> noassign)

instructionVD :: Parser Instruction
instructionVD =  variableDefs <* white

instructionExpr :: Parser Instruction
instructionExpr = INEX <$> expr <* white

instructionNoSemi :: Parser Instruction
instructionNoSemi = instructionVD <|> instructionExpr

instruction :: Parser Instruction
instruction = instructionNoSemi <* char ';' <* white

struct :: Parser CFlow
struct = ST <$> (white *> string "STRUCT" *> white1 *> str) <*> bracket (many1 instruction)

while :: Parser CFlow
while = WH <$> (white *> string "WHILE" *> white *> char '(' *> white *> expr <* white <* char ')' <* white) <*> bracket ( many cflow)

for :: Parser CFlow
for = FOR <$> (white *> string "FOR" *> white *> char '(' *> white *> instruction <* white) <*> (instruction <* white ) <*> (instructionNoSemi <* white <* char ')' <* white) <*>  bracket ( many cflow)

cflow :: Parser CFlow
cflow = try fun <|> Line <$> try instruction <|> for <|> while <|> struct <|> ret

program :: Parser Program
program = many1 cflow

call :: Parser Expr
call = Call <$> (white *> str <* white <* char '(' <* white) <*> many (expr <* optional (char ',')) <* white <* char ')'

strLit :: Parser Expr
strLit = StrLit <$> (white *> char '\"' *>) (many (noneOf "\"")) <* char '\"' <* white

ret :: Parser CFlow
ret = RET <$> (white *> string "RET" *> white1 *>) expr <* char ';' <* white

fun :: Parser CFlow
fun = FUN <$> (functionSkeleton <* white) <*> bracket (many1 cflow)

test :: IO ()
test = do
    x <- readFile "test.ch"
    let t = filter (/= '\n') x
    print $ parse program "" t
